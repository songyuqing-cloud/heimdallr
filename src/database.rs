use crate::cipher::StringCipher;
use crate::{cipher::Cipher, Entry, SodiumArray};
use argon2::password_hash::SaltString;
use icbiadb::{kv, storage::BTreeMap, KvDb};
use serde::{de::DeserializeOwned, Deserialize, Serialize};
use std::io::Write;
use std::path::Path;
use std::{
    io::{self, BufReader, Cursor, Read},
    path::PathBuf,
};
use uuid::Uuid;

pub type MemFile = Cursor<Vec<u8>>;

pub enum CipherFile<'a, R: io::Read + io::Seek> {
    Encrypted {
        path: PathBuf,
        reader: BufReader<R>,
        factors: [u8; 2],
    },
    Uncrypted {
        path: PathBuf,
        cipher: Cipher<'a>,
        string_cipher: StringCipher,
        database: Database,
        factors: [u8; 2],
    },
    None,
}

impl<'a> CipherFile<'a, std::fs::File> {
    pub fn open(path: PathBuf) -> io::Result<Self> {
        let file = std::fs::OpenOptions::new().read(true).open(&path)?;

        let mut reader = std::io::BufReader::new(file);

        let mut stamp = [0u8; 9];
        reader.read_exact(&mut stamp)?;
        let mut factors = [0u8; 2];
        reader.read_exact(&mut factors)?;

        Ok(CipherFile::Encrypted {
            path,
            reader,
            factors,
        })
    }
}

impl<'a, R: io::Read + io::Seek> CipherFile<'a, R> {
    pub fn new(
        path: PathBuf,
        database: Database,
        cipher: Cipher<'a>,
        string_cipher: StringCipher,
        factors: [u8; 2],
    ) -> Self {
        CipherFile::Uncrypted {
            path,
            cipher,
            string_cipher,
            database,
            factors,
        }
    }

    pub fn close() {}

    pub fn read_from(reader: R) -> io::Result<Self>
    where
        R: io::Write + io::Seek,
    {
        let mut reader = std::io::BufReader::new(reader);

        let mut stamp = [0u8; 9];
        reader.read_exact(&mut stamp)?;
        // Read factors and auth types
        let mut factors = [0u8; 2];
        reader.read_exact(&mut factors)?;

        Ok(CipherFile::Encrypted {
            path: PathBuf::from(""),
            reader,
            factors,
        })
    }

    pub fn save(&self, password: &SodiumArray) -> io::Result<()> {
        match self {
            CipherFile::Uncrypted {
                path,
                cipher,
                string_cipher,
                database,
                factors,
            } => {
                let mut bytes = std::io::Cursor::new(Vec::new());
                database.db.commit_to(&mut bytes)?;

                match cipher.encrypt(&bytes.into_inner()) {
                    Ok((data, salt, iv)) => {
                        let file = std::fs::OpenOptions::new()
                            .write(true)
                            .create(true)
                            .truncate(true)
                            .open(&path)?;

                        let mut buf_writer = std::io::BufWriter::new(file);
                        buf_writer.write_all(b"heimdallr")?;
                        buf_writer.write_all(factors)?;
                        buf_writer.write_all(salt.as_bytes())?;
                        buf_writer.write_all(&iv)?;
                        buf_writer.write_all(&data)?;
                        Ok(())
                    }
                    Err(e) => Err(io::Error::new(io::ErrorKind::Other, e.to_string())),
                }
            }
            _ => Err(io::Error::new(
                io::ErrorKind::Other,
                "Vault is not unlocked",
            )),
        }
    }

    pub fn encrypt() {}

    pub fn decrypt(&mut self, password: &SodiumArray) -> io::Result<Self> {
        match self {
            CipherFile::Encrypted {
                ref path,
                ref mut reader,
                factors,
            } => {
                let salt = {
                    let mut buf = vec![0u8; 32];
                    reader.read_exact(&mut buf)?;
                    String::from_utf8(buf).unwrap()
                };
                let iv = {
                    let mut buf = [0u8; 12];
                    reader.read_exact(&mut buf)?;
                    buf
                };
                let data = {
                    let mut data = Vec::new();
                    reader.read_to_end(&mut data)?;
                    data
                };

                let cipher = Cipher::new(&password, &salt, &iv);
                match cipher.decrypt(&data) {
                    Ok(data) => {
                        // TODO, revisit and possibly change string_cipher usage
                        let database = Database::from(data)?;
                        let string_cipher =
                            database.unsafe_get_value::<StringCipher>("string_cipher");
                        Ok(CipherFile::Uncrypted {
                            path: path.clone(),
                            cipher,
                            string_cipher,
                            database,
                            factors: *factors,
                        })
                    }
                    Err(e) => Err(io::Error::new(io::ErrorKind::Other, e.to_string())),
                }
            }
            _ => Err(io::Error::new(
                io::ErrorKind::Other,
                "Vault is already unlocked",
            )),
        }
    }

    pub fn path(&self) -> Option<&PathBuf> {
        match self {
            CipherFile::Encrypted { path, .. } => Some(path),
            CipherFile::Uncrypted { path, .. } => Some(path),
            _ => None,
        }
    }

    pub fn factors(&self) -> [u8; 2] {
        match *self {
            CipherFile::Encrypted { factors, .. } => factors,
            CipherFile::Uncrypted { factors, .. } => factors,
            CipherFile::None => [0, 0],
        }
    }

    pub fn is_open(&self) -> bool {
        matches!(*self, CipherFile::Encrypted { .. })
    }

    pub fn is_decrypted(&self) -> bool {
        matches!(*self, CipherFile::Uncrypted { .. })
    }
}

pub struct Database {
    pub(crate) db: KvDb<BTreeMap>,
}

impl Database {
    pub fn new() -> Self {
        let mut database = Database { db: kv::mem() };
        database.set_group(None, String::from("root"));
        database
    }

    pub fn from(data: Vec<u8>) -> io::Result<Self> {
        Ok(Database {
            db: kv::read_from(Cursor::new(data))?,
        })
    }

    pub fn save(&self, factors: u8, factor_types: usize) -> std::io::Result<()> {
        Ok(())
    }

    pub fn unsafe_set<T: Serialize>(&mut self, key: &str, value: &T) {
        self.db.set(key, value);
    }

    pub fn unsafe_get_value<T: DeserializeOwned>(&self, key: &str) -> T {
        self.db.get_value(key)
    }

    pub fn groups(&self) -> Vec<String> {
        self.db.get_value("groups")
    }

    pub fn is_group(&self, key: &str) -> bool {
        self.db.has_key(&format!("group:{}:children", key))
    }

    pub fn set_group(&mut self, parent: Option<&str>, key: String) {
        if let Some(parent) = parent {
            let mut children = self
                .db
                .get_value::<Vec<String>>(&format!("group:{}:children", parent));
            children.push(key.clone());
            self.db.set(&format!("group:{}:children", parent), children);
        }

        self.db
            .set(&format!("group:{}:children", key), Vec::<String>::new());
    }

    pub fn get_group(&self, key: &str) -> Option<Vec<String>> {
        if let Some(o) = self.db.get(&format!("group:{}:children", key)) {
            return Some(o.extract());
        }

        None
    }

    pub fn delete_group(&mut self, parent: Option<&str>, key: &str) {
        if let Some(parent) = parent {
            let mut children = self
                .db
                .get_value::<Vec<String>>(&format!("group:{}:children", parent));
            if let Some(index) = children.iter().position(|x| x == key) {
                children.remove(index);
                self.db.set(&format!("group:{}:children", parent), children);
            }
        }

        self.db.del(&format!("group:{}:children", key));
    }

    pub fn get_entry(&self, key: &str) -> Option<Entry> {
        if self.db.has_key(&format!("group:{}:children", key)) {
        } else if let Some(o) = self.db.get(key) {
            return Some(o.extract());
        }

        None
    }

    pub fn set_entry(&mut self, group: Option<&str>, entry: Entry) {
        let uuid = Uuid::new_v4().to_string();
        if let Some(group) = group {
            let mut children = if self.db.has_key(&format!("group:{}:children", group)) {
                self.db
                    .get_value::<Vec<String>>(&format!("group:{}:children", group))
            } else {
                Vec::new()
            };

            children.push(uuid.clone());
            self.db.set(format!("group:{}:children", group), children);
            self.db.set(uuid, entry);
        } else {
            let mut children = self.db.get_value::<Vec<String>>("group:root:children");
            children.push(uuid.clone());
            self.db.set("group:root:children", children);
            self.db.set(uuid, entry);
        }
    }

    pub fn delete_entry(&mut self, group: Option<&str>, uuid: &str) {
        if let Some(group) = group {
            if let Some(mut children) = self.get_group(group) {
                if let Some(index) = children.iter().position(|x| x == uuid) {
                    children.remove(index);
                    self.db.set(&format!("group:{}:children", group), children);
                }
            }
            self.db.del(uuid);
        } else {
            if let Some(mut children) = self.get_group("root") {
                if let Some(index) = children.iter().position(|x| x == uuid) {
                    children.remove(index);
                    self.db.set("group:root:children", children);
                }
            }
            self.db.del(uuid);
        }
    }
}
