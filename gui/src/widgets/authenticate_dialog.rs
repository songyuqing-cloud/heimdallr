//use zeroize::Zeroize;
use iced::{button, text_input, Button, Column, Element, Length, Row, Text, TextInput};
use iced_aw::{modal, Card, Modal};

use crate::common::VaultMetaInfo;
use crate::theme;
use crate::widgets::{authenticate_select, AuthWidget};
use heimdallr::SodiumArray;

#[derive(Clone, Debug)]
pub enum AuthenticateDialogMessage {
    OnCancelPress,
    OnUnlockPress,
    PasswordChanged1(String),
    PasswordChanged2(String),
    CloseModal,
    UnlockResult(usize, SodiumArray, SodiumArray),
    AuthWidgetMessage1(authenticate_select::Message),
    AuthWidgetMessage2(authenticate_select::Message),
}

#[derive(Default)]
pub struct ModalState {
    cancel_state: button::State,
    unlock_state: button::State,
    password_state1: text_input::State,
    password_state2: text_input::State,
}

pub struct AuthenticateDialog {
    modal_state: modal::State<ModalState>,
    index: usize,
    vault_meta: VaultMetaInfo,
    password1: SodiumArray,
    password2: SodiumArray,
    authenticator1: AuthWidget,
    authenticator2: AuthWidget,
}

impl Default for AuthenticateDialog {
    fn default() -> Self {
        Self {
            modal_state: Default::default(),
            vault_meta: Default::default(),
            index: 0,
            password1: SodiumArray::new(String::new()).unwrap(),
            password2: SodiumArray::new(String::new()).unwrap(),
            authenticator1: Default::default(),
            authenticator2: Default::default(),
        }
    }
}

impl AuthenticateDialog {
    pub fn set_vault_meta(&mut self, index: usize, vault_meta: VaultMetaInfo) {
        self.index = index;
        self.vault_meta = vault_meta;
    }

    pub fn update(
        &mut self,
        message: AuthenticateDialogMessage,
    ) -> Option<AuthenticateDialogMessage> {
        match message {
            AuthenticateDialogMessage::CloseModal => self.hide(),
            AuthenticateDialogMessage::OnCancelPress => self.hide(),
            AuthenticateDialogMessage::OnUnlockPress => {
                self.hide();

                let psw1 = std::mem::take(&mut self.password1);
                let psw2 = std::mem::take(&mut self.password2);

                return Some(AuthenticateDialogMessage::UnlockResult(
                    self.index, psw1, psw2,
                ));
            }
            AuthenticateDialogMessage::PasswordChanged1(value) => {
                self.password1 = SodiumArray::new(value).unwrap();
                // TODO
                //value.zerioze();
            }
            AuthenticateDialogMessage::PasswordChanged2(value) => {
                self.password2 = SodiumArray::new(value).unwrap();
                // TODO
                //value.zerioze();
            }
            AuthenticateDialogMessage::AuthWidgetMessage1(message) => {
                self.authenticator1.update(message);
            }
            AuthenticateDialogMessage::AuthWidgetMessage2(message) => {
                self.authenticator1.update(message);
            }
            _ => {}
        }

        None
    }

    pub fn show(&mut self) {
        self.modal_state.show(true);
    }

    pub fn hide(&mut self) {
        self.modal_state.show(false);
    }

    pub fn view<'a, M, F>(&'a mut self, content: Element<'a, M>, message: F) -> Element<'a, M>
    where
        M: 'static + Clone,
        F: 'static + Clone + Fn(AuthenticateDialogMessage) -> M,
    {
        // TODO
        // fix ugly hack
        let vault_meta = self.vault_meta.clone();
        let message2 = message.clone();
        let message3 = message.clone();

        // TODO, look for solution
        let password = self.password1.to_string();
        let password2 = self.password2.to_string();

        Modal::new(&mut self.modal_state, content, move |state| {
            let message4 = message3.clone();
            let message5 = message3.clone();
            let lambda = move |value| message4(AuthenticateDialogMessage::PasswordChanged1(value));
            let lambda2 = move |value| message5(AuthenticateDialogMessage::PasswordChanged2(value));
            Card::new(Text::new(&vault_meta.file_name), {
                let mut col = Column::new().push(if vault_meta.factor_types[0] == "Password" {
                    TextInput::new(&mut state.password_state1, "Password", &password, lambda)
                        .size(theme::TEXTINPUT_SIZE)
                        .padding(theme::TEXTINPUT_PADDING)
                        .password()
                } else if vault_meta.factor_types[0] == "Usb drive" {
                    TextInput::new(&mut state.password_state1, "Usb drive", &password, lambda)
                        .size(theme::TEXTINPUT_SIZE)
                        .padding(theme::TEXTINPUT_PADDING)
                        .password()
                } else {
                    TextInput::new(&mut state.password_state1, "Phone", &password, lambda)
                        .size(theme::TEXTINPUT_SIZE)
                        .padding(theme::TEXTINPUT_PADDING)
                        .password()
                });

                if vault_meta.factor_types[1] == "Password" {
                    col = col.push(
                        TextInput::new(&mut state.password_state2, "Password", &password2, lambda2)
                            .size(theme::TEXTINPUT_SIZE)
                            .padding(theme::TEXTINPUT_PADDING)
                            .password(),
                    );
                } else if vault_meta.factor_types[1] == "Usb drive" {
                    col = col.push(
                        TextInput::new(
                            &mut state.password_state2,
                            "Usb drive",
                            &password2,
                            lambda2,
                        )
                        .size(theme::TEXTINPUT_SIZE)
                        .padding(theme::TEXTINPUT_PADDING)
                        .password(),
                    );
                } else if vault_meta.factor_types[1] == "Phone" {
                    col = col.push(
                        TextInput::new(&mut state.password_state2, "Phone", &password2, lambda2)
                            .size(theme::TEXTINPUT_SIZE)
                            .padding(theme::TEXTINPUT_PADDING)
                            .password(),
                    );
                } else {
                }

                // TODO, Zeroize
                // password, password2

                col
            })
            .foot(
                Row::new()
                    .spacing(10)
                    .padding(5)
                    .width(Length::Fill)
                    .push(
                        Button::new(&mut state.cancel_state, Text::new("Cancel"))
                            .on_press(message2(AuthenticateDialogMessage::OnCancelPress))
                            .width(Length::Fill),
                    )
                    .push(
                        Button::new(&mut state.unlock_state, Text::new("Unlock"))
                            .on_press(message2(AuthenticateDialogMessage::OnUnlockPress))
                            .width(Length::Fill),
                    ),
            )
            .max_width(300)
            .on_close(message2(AuthenticateDialogMessage::CloseModal))
            .into()
        })
        .backdrop(message(AuthenticateDialogMessage::CloseModal))
        .on_esc(message(AuthenticateDialogMessage::CloseModal))
        .into()
    }
}
