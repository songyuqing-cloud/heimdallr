#![allow(dead_code, unused_imports, unused_variables)]
#[cfg_attr(not(debug_assertions), windows_subsystem = "windows")]
#[macro_use]
extern crate lazy_static;

mod common;
mod macros;
mod theme;
mod utils;
mod views;
mod widgets;
//mod factory;

use common::Session;
use iced::{
    button, Application, Button, Clipboard, Color, Column, Command, Container, Element, Length,
    Row, Settings,
};
use iced_aw::native::IconText;
use views::{AddView, HomeView, VaultView};

lazy_static! {
    static ref AUTH_TYPE_STRING: Vec<&'static str> = vec!["Password", "Usb drive", "Phone"];
    static ref USB_DRIVE_TEST_OPTIONS: Vec<String> =
        vec![String::from("A:"), String::from("A:"), String::from("A:")];
}

fn main() -> iced::Result {
    MainWindow::run(Settings::default())
}

#[derive(Clone, Debug)]
pub enum Message {
    ChangeToHomeView,
    ChangeToAddView,
    ChangeToVaultView(String),
    CreateVault,
    UnlockVault,

    HomeViewMessage(views::home::Message),
    AddViewMessage(views::AddViewMessage),
    VaultViewMessage(views::vault::Message),
}

pub enum View {
    Home(HomeView),
    AddView(Box<views::AddView>),
    VaultView(views::VaultView),
}

pub struct MainWindow<'a> {
    session: common::Session<'a>,
    views: View,
    home_button_state: button::State,
}

impl<'a> Default for MainWindow<'a> {
    fn default() -> Self {
        let mut session = common::Session::default();
        let home = HomeView::new(&mut session);
        Self {
            session: common::Session::default(),
            views: View::Home(home),
            home_button_state: button::State::default(),
        }
    }
}

impl<'a> Application for MainWindow<'a> {
    type Executor = iced::executor::Default;
    type Message = Message;
    type Flags = ();

    fn new(_flags: ()) -> (Self, Command<Message>) {
        (MainWindow::default(), Command::none())
    }

    fn title(&self) -> String {
        String::from("Heimdallr")
    }

    fn update(&mut self, message: Message, _clipboard: &mut Clipboard) -> Command<Message> {
        match self.views {
            View::Home(ref mut home_view) => {
                if let Message::HomeViewMessage(message) = message {
                    match home_view.update(&mut self.session, message) {
                        Some(Message::ChangeToAddView) => {
                            self.views = View::AddView(Box::new(AddView::default()));
                        }
                        Some(Message::ChangeToVaultView(vault)) => {
                            self.views = View::VaultView(VaultView::new(vault))
                        }
                        _ => {}
                    }
                } else if let Message::ChangeToHomeView = message {
                    self.views = View::Home(HomeView::new(&mut self.session))
                }
            }
            View::AddView(ref mut add_view) => {
                if let Message::AddViewMessage(message) = message {
                    let response = add_view.update(&mut self.session, message);
                    if let Some(Message::ChangeToHomeView) = response {
                        self.views = View::Home(HomeView::new(&mut self.session))
                    }
                }
            }
            View::VaultView(ref mut vault_view) => {
                if let Message::VaultViewMessage(message) = message {
                    match vault_view.update(&mut self.session, message) {
                        Some(Message::ChangeToHomeView) => {
                            self.views = View::Home(HomeView::new(&mut self.session))
                        }
                        _ => {}
                    }
                } else if let Message::ChangeToHomeView = message {
                    self.views = View::Home(HomeView::new(&mut self.session))
                }
            }
        };

        Command::none()
    }

    fn view(&mut self) -> Element<Message> {
        let row = Row::new();

        let main = match self.views {
            View::Home(ref mut home) => home.view(&mut self.session).map(Message::HomeViewMessage),
            View::AddView(ref mut add) => add.view(&mut self.session).map(Message::AddViewMessage),
            View::VaultView(ref mut vault) => {
                vault.view(&mut self.session).map(Message::VaultViewMessage)
            }
        };

        row.push(
            Container::new(
                Column::new().push(
                    Button::new(
                        &mut self.home_button_state,
                        IconText::new("\u{f10b}").size(60),
                    )
                    .on_press(Message::ChangeToHomeView)
                    .style(theme::VaultIconButton),
                ),
            )
            .width(Length::FillPortion(1)),
        )
        .push(Container::new(main).width(Length::FillPortion(10)))
        .into()
    }
}

pub trait ViewTrait: Default {
    type Message;

    fn update(&mut self, session: &mut Session, message: Self::Message) -> Option<Message>;
    fn view(&mut self, session: &mut Session) -> Element<Self::Message>;
}
