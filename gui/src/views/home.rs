use std::path::{Path, PathBuf};

use crate::{
    common::Session,
    theme,
    utils::create_heimdallr_auth,
    widgets::{vault_list::*, AuthenticateDialog, AuthenticateDialogMessage},
    ViewTrait,
};
use heimdallr::Vault;
use iced::{button, Button, Column, Container, Element, HorizontalAlignment, Length, Text};
use iced_aw::{floating_button, FloatingButton, Icon, ICON_FONT};

#[derive(Clone, Debug)]
pub enum Message {
    AddVault,
    RemoveVault(usize),
    VaultListWidgetMessage(VaultListWidgetMessage),
    AuthenticateDialogMessage(AuthenticateDialogMessage),
}

#[derive(Default)]
pub struct HomeView {
    create_value_button_state: button::State,
    vault_views: VaultListWidget,
    authenticate_dialog: AuthenticateDialog,
    show_authenticate_dialog: bool,
}

impl HomeView {
    pub fn new(session: &mut Session) -> Self {
        let vaults = session
            .get_vault_list()
            .drain(..)
            .map(VaultListItemWidget::new)
            .collect::<Vec<VaultListItemWidget>>();

        Self {
            create_value_button_state: button::State::default(),
            vault_views: VaultListWidget::new(vaults),
            authenticate_dialog: Default::default(),
            show_authenticate_dialog: false,
        }
    }
}

impl ViewTrait for HomeView {
    type Message = Message;

    fn update(&mut self, session: &mut Session, message: Message) -> Option<crate::Message> {
        match message {
            Message::VaultListWidgetMessage(message) => {
                if let VaultListWidgetMessage::VaultListItemWidgetMessage(i, ref message) = message
                {
                    if VaultListItemWidgetMessage::LockVault == *message {
                        if let Some((key, mut vault)) = session
                            .unlocked
                            .remove_entry(&self.vault_views.vaults[i].vault_meta.file_name)
                        {
                            vault.lock();
                            self.vault_views.vaults[i].vault_meta.unlocked = false;
                        }
                    } else if VaultListItemWidgetMessage::UnlockVault == *message {
                        self.authenticate_dialog
                            .set_vault_meta(i, self.vault_views.vaults[i].vault_meta.clone());
                        self.authenticate_dialog.show();
                    } else if VaultListItemWidgetMessage::RemoveVaultListing == *message {
                        let vault_item = self.vault_views.vaults.remove(i);
                        session.vault_meta.remove(i);
                        session
                            .db
                            .del(format!("vault:{}", vault_item.vault_meta.file_name))
                            .unwrap();
                    }
                }
                self.vault_views.update(message);
                None
            }
            Message::AuthenticateDialogMessage(message) => {
                if let Some(AuthenticateDialogMessage::UnlockResult(index, pw1, pw2)) =
                    self.authenticate_dialog.update(message)
                {
                    // TODO, refactor
                    let mut vault_meta = &mut session.vault_meta[index];
                    let path = PathBuf::from(&vault_meta.path).join(&vault_meta.file_name);
                    let mut vault = Vault::open(path).unwrap();
                    let authenticators =
                        [create_heimdallr_auth(1, pw1), create_heimdallr_auth(1, pw2)];

                    if let Ok(_) = vault.unlock(&authenticators) {
                        vault_meta.unlocked = true;
                        session
                            .unlocked
                            .insert(vault_meta.file_name.to_string(), vault);
                        return Some(crate::Message::ChangeToVaultView(
                            vault_meta.file_name.to_string(),
                        ));
                    } else {
                        dbg!("DAMNIT");
                    }
                }

                None
            }
            Message::AddVault => Some(crate::Message::ChangeToAddView),
            Message::RemoveVault(i) => None,
        }
    }

    fn view(&mut self, _session: &mut Session) -> Element<Message> {
        let column = Column::new()
            .width(Length::Fill)
            .push(
                Text::new("Vaults")
                    .width(Length::Fill)
                    .size(30)
                    //.color(theme::dark::TEXT)
                    .horizontal_alignment(HorizontalAlignment::Center),
            )
            .push(self.vault_views.view().map(Message::VaultListWidgetMessage));

        let content = FloatingButton::new(
            &mut self.create_value_button_state,
            Container::new(column)
                //.style(theme::dark::MainContainer)
                .padding(10)
                .width(Length::Fill)
                .height(Length::Fill),
            |state| {
                Button::new(
                    state,
                    Text::new(Icon::Plus)
                        .width(Length::Shrink)
                        .height(Length::Shrink)
                        .font(ICON_FONT)
                        .size(39),
                )
                .on_press(Message::AddVault)
                .padding(5)
                .style(theme::RoundedButton)
            },
        )
        .anchor(floating_button::Anchor::SouthEast)
        .offset(20.0)
        .hide(false);

        self.authenticate_dialog
            .view(content.into(), Message::AuthenticateDialogMessage)
            .into()
    }
}
