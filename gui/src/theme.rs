use iced::{Background, Color};

pub const TEXTINPUT_PADDING: u16 = 7;
pub const TEXTINPUT_SIZE: u16 = 17;

pub struct RoundedButton;

impl iced::button::StyleSheet for RoundedButton {
    fn active(&self) -> iced::button::Style {
        iced::button::Style {
            border_radius: 25.0,
            ..iced_aw::style::button::Primary.active()
        }
    }
}

pub struct VaultIconButton;

impl iced::button::StyleSheet for VaultIconButton {
    fn active(&self) -> iced::button::Style {
        iced::button::Style {
            border_width: 0.0,
            background: Some(Background::Color(Color::from_rgb(1.0, 1.0, 1.0))),
            ..Default::default()
        }
    }
}

pub mod dark {
    use super::{Background, Color};

    pub const TEXT: Color = Color::from_rgb(1.0, 1.0, 1.0);

    pub struct VaultIconButton;

    impl iced::button::StyleSheet for VaultIconButton {
        fn active(&self) -> iced::button::Style {
            iced::button::Style {
                border_width: 0.0,
                background: Some(Background::Color(Color::from_rgb(0.1, 0.1, 0.1))),
                ..Default::default()
            }
        }
    }

    pub struct MainContainer;

    impl iced::container::StyleSheet for MainContainer {
        fn style(&self) -> iced::container::Style {
            iced::container::Style {
                background: Some(Background::Color(Color::from_rgb(0.1, 0.1, 0.1))),
                ..Default::default()
            }
        }
    }
}
